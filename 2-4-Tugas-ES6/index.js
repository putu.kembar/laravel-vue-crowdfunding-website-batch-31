// Soal 1

let keliling_persegi_panjang = (...sisi) => {
  let luas = sisi[0] * sisi[1];
  return luas;
};

console.log(keliling_persegi_panjang(10, 5));

//Soal 2

const newFunction = (firstName, lastName) => {
  return {
    firstName: firstName,
    lastName: lastName,
    fullName: () => {
      console.log(`${firstName} ${lastName}`);
    },
  };
};

//Driver Code
newFunction('William', 'Imoh').fullName();

//Soal 3
const newObject = {
  firstName: 'Muhammad',
  lastName: 'Iqbal Mubarok',
  address: 'Jalan Ranamanyar',
  hobby: 'playing football',
};

let { firstName, lastName, address, hobby } = newObject;
console.log(firstName, lastName, address, hobby);

// Soal 4
const west = ['Will', 'Chris', 'Sam', 'Holly'];
const east = ['Gill', 'Brian', 'Noel', 'Maggie'];
const combined = [...west, ...east];
//Driver Code
console.log(combined);

//Soal 5
const planet = 'earth';
const view = 'glass';
let before = `Lorem ${view} dolor sit amet , consectetut adipiscing elit, ${planet}`;

console.log(before);
