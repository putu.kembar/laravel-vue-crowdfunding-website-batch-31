<?php
abstract class Fight {
    public function serang($musuh){
        echo "<br>".$this->nama . " sedang menyerang " . $musuh->nama."<br>"; 
        $musuh->diserang($this);
    }
    public function diserang($musuh)
    {
        echo $this->nama." diserang"."<br>";
        $this->darah = $this->darah - ($musuh->attackPower/$this->deffencePower);
        echo "sisa darah ".$this->nama . " : " .$this->darah;
    }
    protected $attackPower;
    protected $deffencePower;  
}

abstract class Hewan extends Fight{
    protected $nama;
    public function __construct($nama) {
        $this->nama = $nama;
      }
    protected $darah = 50;
    protected $jumlahkaki;
    protected $keahlian ; 
    abstract public function atraksi ();  
        
    public function getInfoHewan(){
        echo "Nama : " . $this->nama;
        echo "<br>";
        echo "Jumlah Kaki : " . $this->jumlahkaki;
        echo "<br>";
        echo "Keahlian : " .$this->keahlian;
        echo "<br>";
        echo "Darah : " . $this->darah."<br>";
        echo "Attack Power : " . $this->attackPower."<br>";
        echo "Deffence Power : " . $this->deffencePower."<br>";
        echo "Jenis Hewan : ". static::class . "<br>";
    }
}

class Harimau extends Hewan{
    
    protected $jumlahkaki = 4;
    protected $keahlian = "lari cepat";
    protected $attackPower = 10;
    protected $deffencePower = 5;
    public function atraksi (){
        echo $this->nama ." sedang ".$this->keahlian."<br>";
    }

}

class Elang extends Hewan{
    
    protected $jumlahkaki = 2;
    protected $keahlian = "terbang tinggi";
    protected $attackPower = 7;
    protected $deffencePower = 8;
    public function atraksi (){
        echo $this->nama . " sedang ".$this->keahlian."<br>";
    }

}




$harimau_1 = new Harimau("harimau_1");
$elang_1 = new Elang("elang_1");


$harimau_1->getInfoHewan();
$harimau_1->atraksi();

echo"<br>----------------------------------------------------<br>";

$elang_1->getInfoHewan();
$elang_1->atraksi();

echo"<br>----------------------------------------------------<br>";

$harimau_1->serang($elang_1);
$elang_1->serang($harimau_1);
$elang_1->serang($harimau_1);
$harimau_1->serang($elang_1);




?>