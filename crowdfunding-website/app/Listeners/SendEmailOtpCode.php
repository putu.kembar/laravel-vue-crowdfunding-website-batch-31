<?php

namespace App\Listeners;

use App\Events\UserRegistered;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendOtpCodeMail;

class SendEmailOtpCode
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserRegistered  $event
     * @return void
     */
    public function handle(UserRegistered $event)
    {
        if ($event->condition == 'register') {
            $pesan = 'wWe are exited to have you get started. First you need to confirm your account. this is your Otp Code : ';
        } elseif ($event->condition =='regenerate') {
            $pesan = 'Regenerate is succesfull, This is your Otp Code : ';
        }

        Mail::to($event->user)->send(new SendOtpCodeMail($event->user , $pesan));
    }
}
