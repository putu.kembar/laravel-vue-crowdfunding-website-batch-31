<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use App\Traits\UsesUuid;

class Role extends Model
{
    use UsesUuid;

    protected $fillable = ['name'];
    

    public function user(){
        return $this->hasMany('App\User');
    }

}
