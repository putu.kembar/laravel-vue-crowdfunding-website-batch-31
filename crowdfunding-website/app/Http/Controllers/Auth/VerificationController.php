<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;

class VerificationController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $request->validate([
            'otp_code' => 'required'
        ]);

        $otp_code = \App\OtpCode::where('otp' , $request->otp_code)->first();

        if (!$otp_code) {
            return response()->json([
                'response_code'     => '01',
                'response_massage'  => 'Kode Otp tidak ditemukan'
            ], 200);
        };

        $now = Carbon::now();

        if ($now > $otp_code->valid_until) {
            return response()->json([
                'response_code'     => '01',
                'response_massage'  => 'Kode OTP anda telah kadaluarsa, silahkan generate ulang'
            ]);
        }

        $user = \App\User::find($otp_code->user_id);
        $user->email_verified_at = Carbon::now();
        $user->save();

        $otp_code->delete();

        $data['user'] = $user;
        return response()->json([
            'response_code' => '00',
            'response_massage'  => 'Email berhasil diverifikasi',
            'data'  => $data
        ]);



    }
}
