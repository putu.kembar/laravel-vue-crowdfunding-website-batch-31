<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\OtpCode;
use Illuminate\Http\Request;
use App\Events\UserRegistered;

class RegenerateOtpCodeController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $request->validate([
            'email' => 'required'
        ]);

        if ( !$user = \App\User::where('email' , $request->email)->first()) {
            return response()->json([
                'error' => 'email tidak ditemukan'
            ]);
        }
        $user = \App\User::where('email' , $request->email)->first();
        $otp_code = \App\OtpCode::where('user_id' , $user->id);
        $otp_code->delete();
        $user->generate_otp_code();

        event(new UserRegistered($user , 'regenerate'));

        $data['user'] = $user;

        return response()->json([
            'response_code' => '00',
            'response_massage' => 'kode otp berasil di regenerate, silahkan cek email anda',
            'data' => $data
        ]);
    }
}
