<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\UsesUuid;

class OtpCode extends Model
{
    protected $guarded = [];

    use UsesUuid;

    public function user(){
        return $this->belongsTo('App\User');
    }
}
