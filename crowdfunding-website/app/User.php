<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use App\Traits\UsesUuid;
use App\OtpCode;
use Carbon\Carbon;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;
    use UsesUuid;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'email', 'name', 'password', 'role_id' , 'photo_profile'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    

    public function role(){
        return $this->belongsTo('App\Role');
    }

    public function otp_code(){
        return $this->hasOne('App\OtpCode');
    }

    public function isAdmin(){
        if ($this->role_id === $this->get_role_admin()) {
            return true;
        }
        return false;
    }

    public function get_role_admin(){
        $role = \App\Role::where('name','admin')->first();

        return $role->id;
    }

    public function get_role_user(){
        $role = Role::where('name','user')->first();

        return $role->id;
    }

    public function generate_otp_code(){
        do {
            $random = mt_rand(100000,999999);
            $check = OtpCode::where('otp' , $random)->first();
        } while ($check);

        $now = Carbon::now();

        $otp_code = OtpCode::updateOrCreate([
            'user_id' => $this->id,
            'otp' => $random,
            'valid_until' => $now->addMinutes(5)
        ]);

    }

    public static function boot(){
        parent::boot();

        static::creating(function($model){
            $model->role_id = $model->get_role_user();
        });

        static::created(function($model){
            $model->generate_otp_code();
        });
    }

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }
}
